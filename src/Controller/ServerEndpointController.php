<?php

namespace Drupal\storybook_server\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Drupal\Core\Render\BareHtmlPageRenderer;

/**
 * Provides an endpoint for Storybook to query.
 * @see https://github.com/storybookjs/storybook/tree/next/app/server
 */
class ServerEndpointController extends ControllerBase {

  /**
   * Render a Twig template from a Storybook component directory.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *
   * @return \Symfony\Component\HttpFoundation\Response|array
   * @throws \Throwable
   * @throws \Twig\Error\RuntimeError
   */
  public function render(Request $request) {
    $response = new Response();
    $themeHandler = \Drupal::service('theme_handler');
    $component = $request->query->get('_storybook');
    $component = preg_replace('/[^[:alnum:]\/_-]/', '', $component);
    $theme = $request->query->get('_drupal_theme');
    $theme = preg_replace('/[^[:alnum:]_-]/', '', $theme);
    if (empty($theme)) {
      $theme = $themeHandler->getDefault();
    }
    $raw = $request->query->getBoolean('_storybook_raw');
    $themePath = $themeHandler->getTheme($theme)->getPath();
    $filename = "$themePath/$component.twig";
    // Check that the filepath isn't trying to break out.
    if (strpos(realpath(\Drupal::root() . "/$filename") , \Drupal::root() . '/' . $themePath) !== 0) {
      $response->setStatusCode(500);
      return $response;
    }
    else {
      // Load the Twig theme engine so we can use twig_render_template().
      include_once \Drupal::root() . '/core/themes/engines/twig/twig.engine';
      $arguments = [];
      foreach ($request->query->all() as $key => $value) {
        $arguments[$key] = json_decode($value, TRUE);
      }
      $markup = (string) twig_render_template($filename, $arguments);

      $build = ['#markup' => $markup];
      $base = [
        '#tag' => 'base',
        '#attributes' => [
          'href' => $request->getSchemeAndHttpHost(),
        ],
      ];
      $build['#attached']['html_head'][] = [$base, 'base'];

      $attachments = \Drupal::service('html_response.attachments_processor');
      $renderer = \Drupal::service('renderer');
      $bareHtmlPageRenderer = new BareHtmlPageRenderer($renderer, $attachments);
      $response = $bareHtmlPageRenderer->renderBarePage($build, 'Page Title', 'markup');
      return $response;
    }
  }
}
