<?php

namespace Drupal\storybook_server\Theme;

use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Theme\ThemeNegotiatorInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Forces a component to render with the selected theme.
 */
class StorybookServerThemeNegotiator implements ThemeNegotiatorInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Construct the StorybookServerThemeNegotiator object.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack used to retrieve the current request.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public function applies(RouteMatchInterface $route_match) {
    $route = $route_match->getRouteObject();
    $theme = $this->getTheme();
    return ($route && $route->getPath() === '/_storybook_server' && !empty($theme));
  }

  /**
   * {@inheritdoc}
   */
  public function determineActiveTheme(RouteMatchInterface $route_match) {
    return $this->getTheme();
  }

  /**
   * Gets the requested theme from URL query parameter.
   *
   * @return string|null
   */
  private function getTheme() {
    $theme = $this->requestStack->getCurrentRequest()->query->get('_drupal_theme');
    $theme = preg_replace('/[^[:alnum:]_-]/', '', $theme);
    return $theme;
  }

}
