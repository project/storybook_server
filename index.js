const fetchStoryHtml = async (url, path, params, context) => {
  // Remove trailing slash.
  url = url.replace(/\/$/, "");

  const regexp = /\.\/web\/themes\/custom\/(.+?)\/(.+)\.stories\.yml/;
  const info = context.parameters.fileName.match(regexp);
  params._drupal_theme = info[1];
  params._storybook = info[2];

  const fetchUrl = new URL(`${url}/_storybook_server`);
  fetchUrl.search = new URLSearchParams({ ...params }).toString();

  // Remove any basic auth embedded into the URL and remove it as it will cause
  // the OPTIONS pre-flight request to fail.
  fetchUrl.username = '';
  fetchUrl.password = '';

  const response = await fetch(fetchUrl, {
    credentials: "include",
  });
  return response.text();
};

const webpackFinal = async (config, { configType }) => {
  // Fix for https://github.com/storybookjs/storybook/issues/7109
  config.optimization.moduleIds = "named";
  return config;
};

module.exports = { fetchStoryHtml, webpackFinal };
