# Storybook Server

[Storybook](https://storybook.js.org/) is an open source tool for building UI
components and pages in isolation. It streamlines UI development, testing, and
documentation.

This module provides an endpoint for Storybook to use with its server rendering
feature, allowing components to be rendered directly with Drupal/Twig.

## Assumptions

- Components are stored in a custom theme under `web/themes/custom/[theme name]`
- You have nodejs installed
- Component templates use the file extension `.twig` (instead of `.html.twig`) -
  this prevents Drupal from picking up templates and rendering them when they
  share a name with a template Drupal already knows about
- Multi-lingual is disabled (@todo!)

## Setup

### Prepare Drupal
- Install this module
  ```console
  composer require drupal/storybook_server
  ```
- In Drupal, either log in as User 1, or grant permission to access Storybook
  Server to a privileged role at `/admin/people/permissions` and log in to
  Drupal with that role.
- If you haven't got a `services.yml`, create one:
  ```console
  cp web/sites/default/default.services.yml web/sites/default/services.yml
  ```
- In `services.yml` set `cors.config.enabled` to `true`, add the Storybook
  site url to `cors.config.allowedOrigins` (by default this is
  `http://localhost:6006` but check the URL when Storybook is started later and
  alter as necessary), and set `cors.config.supportsCredentials` to `true`
  ```yaml
  cors.config:
    enabled: true
    # Specify allowed headers, like 'x-allowed-header'.
    allowedHeaders: []
    # Specify allowed request methods, specify ['*'] to allow all possible ones.
    allowedMethods: []
    # Configure requests allowed from specific origins.
    allowedOrigins: ['http://localhost:6006']
    # Sets the Access-Control-Expose-Headers header.
    exposedHeaders: false
    # Sets the Access-Control-Max-Age header.
    maxAge: false
    # Sets the Access-Control-Allow-Credentials header.
    supportsCredentials: true
  ```
>>>
💥 For better security, put the above settings in a separate `services.yml` file
that is not loaded in production.
>>>

### Create a Story
- In your theme, create a story. For example:
  **`web/themes/custom/mytheme/components/button/button.stories.yml`**
  ```yaml
  title: Bookworm/Status Messages
  stories:
    - name: Default
      args:
        text: Hello, world!
  ```
  **`web/themes/custom/mytheme/components/button/button.twig`**
  ```twig
  <button class="button">{{ text }}</button
  ```

### Install Storybook
- Install Storybook with npx
  ```
  cd mydrupalproject
  npx sb init --type server
  ```
  or if using Yarn >2
  ```
  yarn dlx sb init --type server
  yarn unplug open -AR
  ```
- Install `yaml-loader`
  ```console
  npm install yaml-loader --save-dev
  ```
  or with Yarn
  ```console
  yarn add yaml-loader --dev
  ```
- Edit `.storybook/preview.js`:
  ```
  import { fetchStoryHtml } from '../web/modules/contrib/storybook_server';

  export const parameters = {
    server: {
      // Replace this with your Drupal site URL, or an environment variable.
      url: `https://[your-drupal-site-url]`,
      fetchStoryHtml,
    },
  };
  ```
- Edit `.storybook/main.js` to import configuration and point to your component
  folder(s)
  ```
  const webpackFinal = require('../web/modules/contrib/storybook_server').webpackFinal

  module.exports = {
    "stories": [
      "../web/themes/custom/mytheme/components/**/*.stories.yml"
    ],
    "addons": [
      "@storybook/addon-links",
      "@storybook/addon-essentials"
    ]
  }
  ```
- Start Storybook with `npm run storybook` or `yarn storybook`
